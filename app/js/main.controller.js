app.controller('MainController', function($rootScope, $location, $scope)
{
    var cmdLine_;
    var lineTyped;
    var indexHistory = 0;

    var history = [];
    $scope.init = function () {
        $rootScope.bodyClass = "default";
        cmdLine_ = $('.cmdline');
        lineTyped = $('#lineTyped');
        $scope.typing = "";
        window.addEventListener('click', function(e) {
            cmdLine_.focus();
        }, false);

        document.onkeydown = function(e) {
            var event = window.event ? window.event : e;

            if(event.keyCode === 38) {
                if(indexHistory > 0) {
                    $scope.$apply(function(){
                        $scope.typing = history[--indexHistory];
                    });
                }
            }
            else if(event.keyCode === 40) {
                console.log(event.keyCode);
                if(indexHistory < (history.length-1)) {
                    $scope.$apply(function(){
                        $scope.typing = history[++indexHistory];
                    });
                }
                else if(indexHistory === (history.length-1)) {
                    $scope.$apply(function(){
                        indexHistory++;
                        $scope.typing = "";
                    });
                }
            }
        };
        cmdLine_.focus();
    };

    $scope.command = function(){
        history.push($scope.typing);
        indexHistory = history.length;

        lineTyped.append('<div class="text-typed"><span class="glyphicon glyphicon-menu-right"></span>'+$scope.typing+'</div>');

        try{
            var result = eval($scope.typing)

            lineTyped.append('<div class="text-typed"><b>'+result+'</b></div>');
        }
        catch(e) {

            if($scope.typing === "clear") {
                $scope.clear();
            }
            else if ($scope.typing.startsWith("theme")) {
                $scope.theme($scope.typing);
            }
            else {
                lineTyped.append('<div class="text-typed">Comando: <b>'+$scope.typing+'</b> não encontrado </div>');
            }

        }


        $scope.typing = "";
    };

    $scope.clear = function() {
        lineTyped.html("");
        history = [];
    };

    $scope.theme = function() {
        var tema = $scope.typing.substr($scope.typing.indexOf("--"));
        if(tema in )
        lineTyped.append('<div class="text-typed">Tema atual: <b>'+$rootScope.bodyClass+'</b></div>');
        lineTyped.append('<div class="text-typed">Você deve usar theme --tema_escolhido")</div>');
        lineTyped.append('<div class="text-typed">Temas disponíveis: <i>default, matrix</i></div>');
    };


    $scope.init();


});