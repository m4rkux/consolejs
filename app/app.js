var app = angular.module('app',
    ['ngRoute']);

app.config(function($routeProvider)
{
    $routeProvider
        .when('/', {
            templateUrl : 'app/views/main.html',
            controller     : 'MainController',
        })
        .otherwise ({ redirectTo: '/' });

});